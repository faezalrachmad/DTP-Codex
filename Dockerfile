FROM oziey88/telkomdev-php:7.2-apache-bionic

COPY . /var/www/html/
RUN sed -i 's/Listen 80/Listen 8081/g' /etc/apache2/ports.conf
RUN mkdir /tmp/apache2 && chmod 777 /tmp/apache2
RUN chmod -R 777 /var/run/apache2
RUN chmod -R 777 /var/log/apache2

EXPOSE 8081
