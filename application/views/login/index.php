<!DOCTYPE html>
<html lang="en" class="body-full-height">
	<head>        
		<?php $this->view('layouts/meta'); ?>
	</head>
	<body>

		<div class="login-container">

			<div class="login-box animated fadeInDown">
					<div class="login-body">
					<div class="login-title"><strong>Welcome
					</strong> please login</div>
						<?php 
							if (isset($eroran)) {
								echo $eroran ;
							}
							$class_form = array('class' => 'form-horizontal' );
							echo form_open('login/verify_login', $class_form ); 
						?>
							<div class="form-group">
								<div class="col-md-12">
									<input type="text" id="username" name="username" class="form-control" placeholder="Username"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input type="password" id="passowrd" name="password" class="form-control" placeholder="Password"/>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6"></div>
								<div class="col-md-6">
									<input type="submit" name="submit" id="submit" value="Log In" class="btn btn-info btn-block">
								</div>
							</div>
						<?php echo form_close() ?>
					</div>
				<div class="login-footer">
					<div class="pull-left">
						&copy; 2018 Web Request
					</div>
				</div>
			</div>

		</div>

	</body>
</html>