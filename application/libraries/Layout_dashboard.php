<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Layout_dashboard
{
    private $data;
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    public function show_layout( $folder, $page, $data=null )
    {
        if ( ! file_exists('application/views/'.$folder.'/'.$page.'.php' ) )
        {
            //echo 'application/views/'.$folder.'/'.$page.'.php';
            //die();
            show_404();
        }
        else
        {
            $this->init_menu();

            $data['content'] = $this->CI->load->view($folder.'/'.$page, $data, true);
            $this->CI->load->view('layouts/main.php', $data);
        }
    }

    private function init_menu()
    {        
      // your code to init menus
      // it's a sample code you can init some other part of your page
    }
}